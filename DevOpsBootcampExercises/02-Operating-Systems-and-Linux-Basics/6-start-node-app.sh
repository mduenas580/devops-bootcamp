#!/bin/bash

# Install NodeJS and NPM
echo "Installing NodeJS and NPM..."
if ! command -v node &> /dev/null; then
  curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

# Check NodeJS and NPM versions
echo "Installed NodeJS version:"
node -v
echo "Installed NPM version:"
npm -v

# Download artifact file
echo "Downloading artifact..."
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip downloaded file
echo "Unzipping artifact..."
tar -xvf bootcamp-node-envvars-project-1.0.0.tgz

# Set environment variables
echo "Setting environment variables..."
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

# Change directory
echo "Changing directory..."
cd bootcamp-node-envvars-project-1.0.0

# Install dependencies
echo "Installing dependencies..."
npm install

# Run NodeJS application in background
echo "Starting NodeJS application in background..."
nohup node server.js &

# Print success message
echo "NodeJS application started successfully in the background."
