#!/bin/bash

# Get the current user
current_user=$(whoami)

# Get all running processes
processes=$(ps aux | grep "$current_user" | grep -v grep)

# Ask for user input for sorting
echo "Enter 'memory' to sort by memory usage or 'cpu' to sort by CPU usage:"
read sort_by

# Sort the processes
if [[ "$sort_by" == "memory" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 4 -n) # Sort by memory usage (column 4)
elif [[ "$sort_by" == "cpu" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 3 -n) # Sort by CPU usage (column 3)
else
  echo "Invalid input. Please enter 'memory' or 'cpu'."
  exit 1
fi

# Print the sorted processes
echo "Processes running for user '$current_user' (sorted by $sort_by):"
echo "$sorted_processes"
