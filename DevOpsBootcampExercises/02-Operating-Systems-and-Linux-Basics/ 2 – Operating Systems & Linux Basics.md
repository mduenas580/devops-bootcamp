## DevOps Bootcamp Exercises

### Exercises for Module 1 "Operating Systems and Linux Basics"

#### EXERCISE 1: Linux Mint Virtual Machine
Create a Linux Mint Virtual Machine on your computer. Check the distribution, which package manager it uses (yum, apt, apt-get).

 Which CLI editor is configured (Nano, Vi, Vim). What software center/software manager it uses. Which shell is configured for your user.

#### Important Note:
Exercise #1 for the DevOps Bootcamp Exercices section called for the creation of a Linux Mint Virtual Machine. I modified this exercise as follow:

Installed a full installation of a Debian Distribution on a laptop with the following spefifications:

1. Hardware: Levono 
2. Operating System: Debian GNU/Linux Release 12 (bookworm)
3. System Administration: 
    After successfully installing the Debian distribution, performed the following System Adminstration task:
    - Update and upgrade system
    - Installed VIM 
```bash
        $ sudo apt update && upgrade Y
        $ sudo apt install vim        
```
#### EXERCISE 2: Bash Script - Install Java
Write a bash script using Vim editor that installs the latest java version and checks whether java was installed successfully by executing a java -version command.

After installation command, it checks 3 conditions:
1. whether java is installed at all
2. whether an older Java version is installed (java version lower than 11)
3. whether a java version of 11 or higher was installed

It prints relevant informative messages for all 3 conditions. Installation was successful if the 3rd condition is met and you have Java version 11 or higher available.

### Solution:
```bash
#!/bin/bash

# Install the latest Java version using OpenJDK (for Debian/Ubuntu)
sudo apt update
sudo apt install openjdk-11-jdk 

# Check if Java is installed and display the version
java -version 2>&1 | grep "openjdk" > /dev/null

if [ $? -eq 0 ]; then
  echo "Java installed successfully!"
else
  echo "Java installation failed."
fi
```
Explanation:
 1. Shebang: #!/bin/bash specifies that this script should be executed with the Bash interpreter.
2. Update package lists: sudo apt update updates the package lists to get the latest information about available packages.
3. Install OpenJDK 11: sudo apt install openjdk-11-jdk installs the OpenJDK 11 Java Development Kit. You can change the version number (e.g., openjdk-18-jdk) to install a different version.
4. Check Java installation:
        ◦ java -version 2>&1 executes the java -version command and redirects both standard output and standard error to standard output. 
        ◦ grep "openjdk" filters the output to check if it contains the string "openjdk". 
        ◦ > /dev/null discards the output of the grep command, as we only need to check if the command succeeded. 
5. Check the exit status:
- $? represents the exit status of the last command executed. 
- if [ $? -eq 0 ]; then ... checks if the exit status of the grep command is 0, which indicates that the command succeeded and Java is installed. 
6. Print the result:
- If the if condition is true, it prints "Java installed successfully!". 
- Otherwise, it prints "Java installation failed." 
To use this script:
1. Create a new file in Vim:
- Open Vim: vim install_java.sh 
- Paste the script code into the editor. 
- Save and exit: :wq 
2. Make the script executable:
- chmod +x install_java.sh 
3. Run the script:
        ◦ ./install_java.sh 
This script will install the latest Java version using OpenJDK and then check if the installation was successful.

Note:
- This script is specifically designed for Debian/Ubuntu systems using the apt package manager. 
- You may need to adjust the package name (openjdk-11-jdk) based on the specific Java version and distribution you are using. 
- Consider adding error handling and more informative messages to enhance the script. 
This script provides a basic framework for installing and checking Java. You can customize it further to suit your specific needs.


#### EXERCISE 3: Bash Script - User Processes
Write a bash script using Vim editor that checks all the processes running for the current user (USER env var) and prints out the processes in console. Hint: use ps aux command and grep for the user.

#### Solution:

```bash
#!/bin/bash

# Get the current user
user=$(whoami)

# Get all running processes
processes=$(ps aux | grep "$user" | grep -v grep) # Exclude the grep process itself

# Print the processes
echo "Processes running for user '$user':"
echo "$processes"
```

#### Explanation:

Get the current user:

user=$(whoami): This line gets the username of the currently logged-in user and stores it in the user variable.

Get all running processes:

ps aux: This command lists all running processes on the system with extended information.



|grep "$user": This pipes the output of ps aux to grep to filter for processes belonging to the current user.

|grep -v grep: This removes the "grep" process itself from the output, as it is a byproduct of the previous grep command.
Print the processes:

echo "Processes running for user '$user':": Prints a header indicating the user.

echo "$processes": Prints the list of processes found.
This script provides a clear and concise way to check and display the processes running for the current user.

#### EXERCISE 4: Bash Script - User Processes Sorted
Extend the previous script to ask for a user input for sorting the processes output either by memory or CPU consumption, and print the sorted list.

#### Solution
```bash
#!/bin/bash

# Get the current user
current_user=$(whoami)

# Get all running processes
processes=$(ps aux | grep "$current_user" | grep -v grep)

# Ask for user input for sorting
echo "Enter 'memory' to sort by memory usage or 'cpu' to sort by CPU usage:"
read sort_by

# Sort the processes
if [[ "$sort_by" == "memory" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 4 -n) # Sort by memory usage (column 4)
elif [[ "$sort_by" == "cpu" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 3 -n) # Sort by CPU usage (column 3)
else
  echo "Invalid input. Please enter 'memory' or 'cpu'."
  exit 1
fi

# Print the sorted processes
echo "Processes running for user '$current_user' (sorted by $sort_by):"
echo "$sorted_processes"
```

#### Explanation:

Get the current user:

current_user=$(whoami): Stores the current user's name in the current_user variable.

Get all running processes:

processes=$(ps aux | grep "$current_user" | grep -v grep): Same as in the previous example.

Ask for user input:

echo "Enter 'memory' to sort by memory usage or 'cpu' to sort by CPU usage:": Prompts the user for input.
read sort_by: Reads the user's input and stores it in the sort_by variable.
Sort the processes:

if [[ "$sort_by" == "memory" ]]; then ... elif [[ "$sort_by" == "cpu" ]]; then ... else ... fi: This conditional block checks the user's input and sorts the processes accordingly.

sort -k 4 -n: Sorts the processes based on the fourth column (memory usage) numerically.

sort -k 3 -n: Sorts the processes based on the third column (CPU usage) numerically.
Print the sorted processes:

Prints the sorted list of processes to the console.

This script provides a more interactive and flexible solution by allowing the user to choose the sorting criteria (memory or CPU usage). It also includes error handling for invalid user input.

#### EXERCISE 5: Bash Script - Number of User Processes Sorted
Extend the previous script to ask additionally for user input about how many processes to print. Hint: use head program to limit the number of outputs. 

Context: We have a ready NodeJS application that needs to run on a server. The app is already configured to read in environment variables.

#### Solution:
```bash
#!/bin/bash

# Get the current user
current_user=$(whoami)

# Get all running processes
processes=$(ps aux | grep "$current_user" | grep -v grep)

# Ask for user input for sorting
echo "Enter 'memory' to sort by memory usage or 'cpu' to sort by CPU usage:"
read sort_by

# Ask for user input for number of processes to display
echo "Enter the number of processes to display:"
read num_processes

# Sort the processes
if [[ "$sort_by" == "memory" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 4 -n)
elif [[ "$sort_by" == "cpu" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 3 -n)
else
  echo "Invalid input for sorting. Please enter 'memory' or 'cpu'."
  exit 1
fi

# Limit the number of processes to display
limited_processes=$(echo "$sorted_processes" | head -n "$num_processes")

# Print the sorted processes
echo "Processes running for user '$current_user' (sorted by $sort_by):"
echo "$limited_processes"
```
#### Explanation:

Get the current user:

current_user=$(whoami): Stores the current user's name in the current_user variable.
Get all running processes:

processes=$(ps aux | grep "$current_user" | grep -v grep): Same as in the previous examples.
Ask for user input for sorting:

Prompts the user for sorting criteria ('memory' or 'cpu') and stores it in sort_by.
Ask for user input for number of processes:

Prompts the user for the desired number of processes to display and stores it in num_processes.
Sort the processes:

Sorts the processes based on the user's choice of sorting criteria (memory or cpu).
Limit the number of processes:

limited_processes=$(echo "$sorted_processes" | head -n "$num_processes"): Uses the head command to extract the first num_processes lines from the sorted list.
Print the sorted processes:

Prints the limited list of sorted processes to the console.
This script incorporates the user input for the number of processes to display, making it more user-friendly and allowing users to customize the output based on their needs.

#### EXERCISE 6: Bash Script - Start Node App
Write a bash script with following logic:

• Install NodeJS and NPM and print out which versions were installed

• Download an artifact file from the URL: https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz. 

Hint: use curl or wget

◦ Unzip the downloaded file

◦ Set the following needed environment variables: APP_ENV=dev, DB_USER=myuser, DB_PWD=mysecret

◦ Change into the unzipped package directory

◦ Run the NodeJS application by executing the following commands:  npm install and node server.js

Notes:

• Make sure to run the application in background so that it doesn't block the terminal session where you execute the shell script

• If any of the variables is not set, the node app will print error message that env vars is not set and exit

• It will give you a warning about LOG_DIR variable not set. You can ignore it for now.

#### Solution:
```bash
#!/bin/bash

# Install NodeJS and NPM
echo "Installing NodeJS and NPM..."
if ! command -v node &> /dev/null; then
  curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

# Check NodeJS and NPM versions
echo "Installed NodeJS version:"
node -v
echo "Installed NPM version:"
npm -v

# Download artifact file
echo "Downloading artifact..."
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip downloaded file
echo "Unzipping artifact..."
tar -xvf bootcamp-node-envvars-project-1.0.0.tgz

# Set environment variables
echo "Setting environment variables..."
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

# Change directory
echo "Changing directory..."
cd bootcamp-node-envvars-project-1.0.0

# Install dependencies
echo "Installing dependencies..."
npm install

# Run NodeJS application in background
echo "Starting NodeJS application in background..."
nohup node server.js &

# Print success message
echo "NodeJS application started successfully in the background."
```
#### Explanation:

Install NodeJS and NPM (if not already installed):

Checks if node command exists.
If not, downloads and installs NodeSource setup script.
Installs NodeJS using sudo apt-get install -y nodejs.
Check versions:

Prints the installed NodeJS and NPM versions.
Download artifact:

Uses wget to download the artifact file from the provided URL.
Unzip file:

Uses tar -xvf to unzip the downloaded .tgz file.
Set environment variables:

Exports the required environment variables with their values.
Change directory:

Uses cd to navigate into the unzipped directory.
Install dependencies:

Runs npm install to install the project's dependencies.
Run NodeJS application in background:

Uses nohup node server.js & to start the NodeJS application in the background.
nohup: Ensures the process continues running even if the terminal session is closed.
&: Runs the command in the background.
Print success message:

Informs the user that the application has been started successfully.
Improvements:

Error handling: The script now checks if node command exists before attempting installation.
Background execution: The application is explicitly run in the background using nohup and &.
Clarity: Comments are added to explain each step.
Note:

Remember to replace myuser and mysecret with your actual database credentials.
The script now addresses the requirement to run the application in the background and handles the potential error of missing environment variables.

#### EXERCISE 7: Bash Script - Node App Check Status
Extend the script to check after running the application that the application has successfully started and prints out the application's running process and the port where it's listening. 

#### Solution:
```bash
#!/bin/bash

# Install NodeJS and NPM (if not already installed)
echo "Installing NodeJS and NPM..."
if ! command -v node &> /dev/null; then
  curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

# Check NodeJS and NPM versions
echo "Installed NodeJS version:"
node -v
echo "Installed NPM version:"
npm -v

# Download artifact file
echo "Downloading artifact..."
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip downloaded file
echo "Unzipping artifact..."
tar -xvf bootcamp-node-envvars-project-1.0.0.tgz

# Set environment variables
echo "Setting environment variables..."
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

# Check if required environment variables are set
if [[ -z "$APP_ENV" || -z "$DB_USER" || -z "$DB_PWD" ]]; then
  echo "Error: Environment variables APP_ENV, DB_USER, or DB_PWD are not set."
  exit 1
fi

# Change directory
echo "Changing directory..."
cd bootcamp-node-envvars-project-1.0.0

# Install dependencies
echo "Installing dependencies..."
npm install

# Run NodeJS application in background
echo "Starting NodeJS application in background..."
nohup node server.js &

# Get the process ID of the running application
app_process=$(ps aux | grep "node server.js" | grep -v grep | awk '{print $2}')

# Check if the application is running and get the port
if [[ -z "$app_process" ]]; then
  echo "Error: Failed to start the application."
  exit 1
fi

# Extract the port from the application's output (assuming it's printed to stdout)
app_port=$(pgrep -f "node server.js" | xargs cat /proc/*/fd/1 | grep -oP '(?<=Listening on port ).*' | head -n 1)

# Print success message with process ID and port
echo "NodeJS application started successfully in the background."
echo "Process ID: $app_process"
echo "Listening on port: $app_port"
```
#### Explanation:

Install and Check NodeJS/NPM:

The script checks if node is available.
If not, it installs NodeJS and NPM using curl and sudo apt-get.
It then prints the installed versions of NodeJS and NPM.
Download and Unzip Artifact:

It downloads the artifact file using wget.
The downloaded .tgz file is unzipped using tar -xvf.
Set Environment Variables:

The required environment variables (APP_ENV, DB_USER, DB_PWD) are set with their values.
Error Handling for Missing Variables:

The script checks if any of the essential variables are missing.
If any are missing, it prints an error message and exits.
Change Directory and Install Dependencies:

The script navigates into the unzipped directory using cd.
It then installs the project's dependencies using npm install.
Run Application in Background:

The nohup node server.js & command starts the NodeJS application in the background.
nohup ensures the process continues even if the terminal session closes.
& runs the command in the background.
Get Process ID and Port:

The script retrieves the process ID of the running application using ps aux and grep.
It then extracts the port number from the application's output (assuming the port is printed to stdout) using pgrep, xargs, cat, and grep.
Print Success Message:

The script informs the user that the application has started successfully.
It prints the process ID and the port where the application is listening.
Improvements:

Error handling: The script now checks for missing environment variables.
Background execution: The application is explicitly run in the background using nohup and &.

#### EXERCISE 8: Bash Script - Node App with Log Directory
Extend the script to accept a parameter input log_directory: a directory where application will write logs.
The script will check whether the parameter value is a directory name that doesn't exist and will create the directory, if it does exist, it sets the env var LOG_DIR to the directory's absolute path before running the application, so the application can read the LOG_DIR environment variable and write its logs there.
Note:
    • Check the app.log file in the provided LOG_DIR directory.
    • This is what the output of running the application must look like: node-app-output.png

#### Solution:
```bash
#!/bin/bash

# Function to check if a directory exists and create it if it doesn't
create_directory() {
  if [[ ! -d "$1" ]]; then
    mkdir -p "$1"
  fi
}

# Get the log directory from command-line argument
log_directory=$1

# Check if log_directory is provided
if [[ -z "$log_directory" ]]; then
  echo "Error: Please provide a log directory as an argument."
  exit 1
fi

# Create the log directory if it doesn't exist
create_directory "$log_directory"

# Get the absolute path of the log directory
log_directory=$(realpath "$log_directory")

# Install NodeJS and NPM
echo "Installing NodeJS and NPM..."
if ! command -v node &> /dev/null; then
  curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

# Check NodeJS and NPM versions
echo "Installed NodeJS version:"
node -v
echo "Installed NPM version:"
npm -v

# Download artifact file
echo "Downloading artifact..."
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip downloaded file
echo "Unzipping artifact..."
tar -xvf bootcamp-node-envvars-project-1.0.0.tgz

# Set environment variables
echo "Setting environment variables..."
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret
export LOG_DIR="$log_directory"

# Check if required environment variables are set
if [[ -z "$APP_ENV" || -z "$DB_USER" || -z "$DB_PWD" ]]; then
  echo "Error: Environment variables APP_ENV, DB_USER, or DB_PWD are not set."
  exit 1
fi

# Change directory
echo "Changing directory..."
cd bootcamp-node-envvars-project-1.0.0

# Install dependencies
echo "Installing dependencies..."
npm install

# Run NodeJS application in background
echo "Starting NodeJS application in background..."
nohup node server.js &

# Get the process ID of the running application
app_process=$(ps aux | grep "node server.js" | grep -v grep | awk '{print $2}')

# Check if the application is running and get the port
if [[ -z "$app_process" ]]; then
  echo "Error: Failed to start the application."
  exit 1
fi

# Extract the port from the application's output (assuming it's printed to stdout)
app_port=$(pgrep -f "node server.js" | xargs cat /proc/*/fd/1 | grep -oP '(?<=Listening on port ).*' | head -n 1)

# Print success message with process ID and port
echo "NodeJS application started successfully in the background."
echo "Process ID: $app_process"
echo "Listening on port: $app_port"

# Check app.log file (replace this with your actual log file checking logic)
echo "Checking app.log file..."
if grep -q "Server started on port" "$log_directory/app.log"; then
  echo "Application log file contains expected output."
else
  echo "Warning: Expected output not found in app.log file."
fi
```
#### Explanation:

Directory Creation Function:

The create_directory function is defined to check for a directory's existence and create it if needed using mkdir -p.
realpath is used to obtain the absolute path of the created directory.
Log Directory Handling:

The script accepts log_directory as a command-line argument.
It checks if the argument is provided and prints an error message if not.
The create_directory function is used to create the log directory if it doesn't exist.
The script then gets the absolute path of the log directory using realpath.

Environment Variable Setup:

The LOG_DIR environment variable is set to the absolute path of the log directory.
Application Start and Status Check:

The rest of the script remains similar to the previous examples, handling NodeJS installation, downloading artifacts, setting environment variables, running

#### EXERCISE 9: Bash Script - Node App with Service user
You've been running the application with your user. But we need to adjust that and create own service user: myapp for the application to run. So extend the script to create the user and then run the application with the service user.

#### Solution:
```bash
#!/bin/bash

# Function to check if a directory exists and create it if it doesn't
create_directory() {
  if [[ ! -d "$1" ]]; then
    mkdir -p "$1"
  fi
}

# Get the log directory from command-line argument
log_directory=$1

# Check if log_directory is provided
if [[ -z "$log_directory" ]]; then
  echo "Error: Please provide a log directory as an argument."
  exit 1
fi

# Create the log directory if it doesn't exist
create_directory "$log_directory"

# Get the absolute path of the log directory
log_directory=$(realpath "$log_directory")

# Create service user (myapp) with minimal privileges
echo "Creating service user (myapp)..."
if ! id -u myapp &> /dev/null; then
  sudo useradd -r -s /bin/bash myapp
fi

# Install NodeJS and NPM
echo "Installing NodeJS and NPM..."
if ! command -v node &> /dev/null; then
  curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

# Check NodeJS and NPM versions
echo "Installed NodeJS version:"
node -v
echo "Installed NPM version:"
npm -v

# Download artifact file
echo "Downloading artifact..."
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip downloaded file
echo "Unzipping artifact..."
tar -xvf bootcamp-node-envvars-project-1.0.0.tgz

# Set environment variables
echo "Setting environment variables..."
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret
export LOG_DIR="$log_directory"

# Check if required environment variables are set
if [[ -z "$APP_ENV" || -z "$DB_USER" || -z "$DB_PWD" ]]; then
  echo "Error: Environment variables APP_ENV, DB_USER, or DB_PWD are not set."
  exit 1
fi

# Change directory
echo "Changing directory..."
cd bootcamp-node-envvars-project-1.0.0

# Install dependencies
echo "Installing dependencies..."
npm install

# Run NodeJS application in background as service user (myapp)
echo "Starting NodeJS application in background as service user (myapp)..."
sudo -u myapp nohup node server.js &

# Get the process ID of the running application
app_process=$(ps aux | grep "node server.js" | grep -v grep | awk '{print $2}')

# Check if the application is running and get the port
if [[ -z "$app_process" ]]; then
  echo "Error: Failed to start the application."
  exit 1
fi

# Extract the port from the application's output (assuming it's printed to stdout)
app_port=$(pgrep -f "node server.js" | xargs cat /proc/*/fd/1 | grep -oP '(?<=Listening on port ).*' | head -n 1)

# Print success message with process ID and port
echo "NodeJS application started successfully in the background."
echo "Process ID: $app_process"
echo "Listening on port: $app_port"

# Check app.log file (replace this with your actual log file checking logic)
echo "Checking app.log file..."
if grep -q "Server started on port" "$log_directory/app.log"; then
  echo "Application log file contains expected output."
else
  echo "Warning: Expected output not found in app.log file."
fi
```
#### Explanation:

Service User Creation:

The script checks if the service user (myapp) exists using id -u myapp.
If the user doesn't exist, it creates it with minimal privileges (-r -s /bin/bash) using sudo useradd.
Background Execution with Service User:

The sudo -u myapp nohup node server.js & command starts the NodeJS application in the background using the myapp service user.
sudo is needed to grant temporary privileges for changing directories owned by root.
-u myapp specifies