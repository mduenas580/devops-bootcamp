#!/bin/bash

# Install NodeJS and NPM (if not already installed)
echo "Installing NodeJS and NPM..."
if ! command -v node &> /dev/null; then
  curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

# Check NodeJS and NPM versions
echo "Installed NodeJS version:"
node -v
echo "Installed NPM version:"
npm -v

# Download artifact file
echo "Downloading artifact..."
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip downloaded file
echo "Unzipping artifact..."
tar -xvf bootcamp-node-envvars-project-1.0.0.tgz

# Set environment variables
echo "Setting environment variables..."
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

# Check if required environment variables are set
if [[ -z "$APP_ENV" || -z "$DB_USER" || -z "$DB_PWD" ]]; then
  echo "Error: Environment variables APP_ENV, DB_USER, or DB_PWD are not set."
  exit 1
fi

# Change directory
echo "Changing directory..."
cd bootcamp-node-envvars-project-1.0.0

# Install dependencies
echo "Installing dependencies..."
npm install

# Run NodeJS application in background
echo "Starting NodeJS application in background..."
nohup node server.js &

# Get the process ID of the running application
app_process=$(ps aux | grep "node server.js" | grep -v grep | awk '{print $2}')

# Check if the application is running and get the port
if [[ -z "$app_process" ]]; then
  echo "Error: Failed to start the application."
  exit 1
fi

# Extract the port from the application's output (assuming it's printed to stdout)
app_port=$(pgrep -f "node server.js" | xargs cat /proc/*/fd/1 | grep -oP '(?<=Listening on port ).*' | head -n 1)

# Print success message with process ID and port
echo "NodeJS application started successfully in the background."
echo "Process ID: $app_process"
echo "Listening on port: $app_port"
