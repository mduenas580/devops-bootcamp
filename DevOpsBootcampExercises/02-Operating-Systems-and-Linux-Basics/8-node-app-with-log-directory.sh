#!/bin/bash

# Function to check if a directory exists and create it if it doesn't
create_directory() {
  if [[ ! -d "$1" ]]; then
    mkdir -p "$1"
  fi
}

# Get the log directory from command-line argument
log_directory=$1

# Check if log_directory is provided
if [[ -z "$log_directory" ]]; then
  echo "Error: Please provide a log directory as an argument."
  exit 1
fi

# Create the log directory if it doesn't exist
create_directory "$log_directory"

# Get the absolute path of the log directory
log_directory=$(realpath "$log_directory")

# Install NodeJS and NPM
echo "Installing NodeJS and NPM..."
if ! command -v node &> /dev/null; then
  curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
fi

# Check NodeJS and NPM versions
echo "Installed NodeJS version:"
node -v
echo "Installed NPM version:"
npm -v

# Download artifact file
echo "Downloading artifact..."
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip downloaded file
echo "Unzipping artifact..."
tar -xvf bootcamp-node-envvars-project-1.0.0.tgz

# Set environment variables
echo "Setting environment variables..."
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret
export LOG_DIR="$log_directory"

# Check if required environment variables are set
if [[ -z "$APP_ENV" || -z "$DB_USER" || -z "$DB_PWD" ]]; then
  echo "Error: Environment variables APP_ENV, DB_USER, or DB_PWD are not set."
  exit 1
fi

# Change directory
echo "Changing directory..."
cd bootcamp-node-envvars-project-1.0.0

# Install dependencies
echo "Installing dependencies..."
npm install

# Run NodeJS application in background
echo "Starting NodeJS application in background..."
nohup node server.js &

# Get the process ID of the running application
app_process=$(ps aux | grep "node server.js" | grep -v grep | awk '{print $2}')

# Check if the application is running and get the port
if [[ -z "$app_process" ]]; then
  echo "Error: Failed to start the application."
  exit 1
fi

# Extract the port from the application's output (assuming it's printed to stdout)
app_port=$(pgrep -f "node server.js" | xargs cat /proc/*/fd/1 | grep -oP '(?<=Listening on port ).*' | head -n 1)

# Print success message with process ID and port
echo "NodeJS application started successfully in the background."
echo "Process ID: $app_process"
echo "Listening on port: $app_port"

# Check app.log file (replace this with your actual log file checking logic)
echo "Checking app.log file..."
if grep -q "Server started on port" "$log_directory/app.log"; then
  echo "Application log file contains expected output."
else
  echo "Warning: Expected output not found in app.log file."
fi
