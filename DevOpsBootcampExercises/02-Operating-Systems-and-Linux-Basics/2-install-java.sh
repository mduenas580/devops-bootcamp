#!/bin/bash

# Install the latest Java version using OpenJDK (for Debian/Ubuntu)
sudo apt update
sudo apt install openjdk-11-jdk 

# Check if Java is installed and display the version
java -version 2>&1 | grep "openjdk" > /dev/null

if [ $? -eq 0 ]; then
  echo "Java installed successfully!"
else
  echo "Java installation failed."
fi

