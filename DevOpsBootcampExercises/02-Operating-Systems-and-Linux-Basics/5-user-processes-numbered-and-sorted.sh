#!/bin/bash

# Get the current user
current_user=$(whoami)

# Get all running processes
processes=$(ps aux | grep "$current_user" | grep -v grep)

# Ask for user input for sorting
echo "Enter 'memory' to sort by memory usage or 'cpu' to sort by CPU usage:"
read sort_by

# Ask for user input for number of processes to display
echo "Enter the number of processes to display:"
read num_processes

# Sort the processes
if [[ "$sort_by" == "memory" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 4 -n)
elif [[ "$sort_by" == "cpu" ]]; then
  sorted_processes=$(echo "$processes" | sort -k 3 -n)
else
  echo "Invalid input for sorting. Please enter 'memory' or 'cpu'."
  exit 1
fi

# Limit the number of processes to display
limited_processes=$(echo "$sorted_processes" | head -n "$num_processes")

# Print the sorted processes
echo "Processes running for user '$current_user' (sorted by $sort_by):"
echo "$limited_processes"
