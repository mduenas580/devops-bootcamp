#!/bin/bash

# Get the current user
user=$(whoami)

# Get all running processes
processes=$(ps aux | grep "$user" | grep -v grep) # Exclude the grep process itself

# Print the processes
echo "Processes running for user '$user':"
echo "$processes"
