## DevOps Bootcamp Curriculum

0 – Module Handout & Checklist\
1 – Introduction to DevOps\
2 – Operating Systems & Linux Basics\
3 – Version Control with Git\
Bonus – Databases\
4 – Build Tools and Package Manager Tools\
5 – Cloud & Infrastructure as Service Basics with DigitalOcean\
6 - Artifact Repository Manager with Nexus\
7 - Containers with Docker\
8 - Build Automation & CI/CD with Jenkins\
9 - AWS Services\
10 - Container Orchestration with Kubernetes\
11 - Kubernetes on AWS - EKS\
12 - Infrastructure as Code with Terraform\
13 - Programming Basics with Python\
14 - Automation with Python\
15 - Configuration Management with Ansible\
16 - Monitoring with Prometheus